import React, {Component, Fragment} from 'react';
import Modal from "../../components/UI/Modal/Modal";
import TextInfo from "../../components/Info/TextInfo/TextInfo";
import Alert from "../../components/UI/Alert/Alert";


class PopUp extends Component{


  state = {
    purchasing: false,
    purchasable: true,
  };

  closeHandler = () =>{
    this.setState({purchasable: false})
  };

  purchaseOpened = () =>{
    this.setState({purchasing: true})
  };

  purchaseContinued = () =>{
    alert('Cool !!!')
  };

  purchaseClosed = () =>{
    this.setState({purchasing: false})
  };

  render() {
    return (
      <Fragment>
        <button onClick={this.purchaseOpened}>Open Modal Window</button>
        <Modal
          show={this.state.purchasing}

        >
          <TextInfo
            close={this.purchaseClosed}
            open={this.purchaseContinued}
            title='This is text for Modal Title'

          >This is text for Modal Title</TextInfo>
        </Modal>
        <Alert
          show={this.state.purchasable}
          type="warning"
          dismiss={this.closeHandler}
          // click={}
        >This is a warning type alert</Alert>
        <Alert
          show={this.state.purchasable}
          type="danger"
          dismiss={this.closeHandler}

        >This is a danger type alert</Alert>
        <Alert
          show={this.state.purchasable}
          type="success"

        >This is a success type alert</Alert>
        <Alert
          show={this.state.purchasable}
          type="primary"

        >This is a primary type alert</Alert>
      </Fragment>
    );
  }
}

export default PopUp;