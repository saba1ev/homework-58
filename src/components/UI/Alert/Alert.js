import React, {Fragment} from 'react';
import Button from "../Button/Button";
import './Alert.css'

const Alert = props => {
  return (
    <Fragment>
      <div
        className={["Alert",  props.type].join(" ")}
        style={{
          transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: props.show ? '1' : '0'
        }}
      >

        {props.children}
        {props.dismiss ? <Button clicked={props.dismiss}>X</Button>: null}
      </div>
    </Fragment>


  )
  ;
};

export default Alert;