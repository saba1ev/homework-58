import React, {Fragment} from 'react';
import Button from "../../UI/Button/Button";

const TextInfo = props => {
  return (
   <Fragment>
     <div className='Grey-line'>
       <h2>{props.title}</h2>
       <button
         onClick={props.close}
       >×
       </button>
     </div>
     <hr/>
     <div className='Modal-body'>
       <p>{props.children}</p>
     </div>
     <hr/>
     <div>
       <Button clicked={props.close}>Close</Button>
       <Button clicked={props.open}>Open</Button>
     </div>
   </Fragment>
  );
};

export default TextInfo;