import React, { Component } from 'react';
import './App.css';
import PopUp from "./Container/PopUp/PopUp";

class App extends Component {

  render() {
    return (
      <div className="App">
        <PopUp />
      </div>
    );
  }
}

export default App;
